class WebSocketConnection {
  constructor(address) {
    this.address = address;
    this.webSocket = null;
    this.roomId = null;
    this.observers = [];
  }

  connect(roomId, username) {
    this.webSocket = new WebSocket(
      `${this.address}/roomId=${roomId}&username=${username}`
    );
    this.roomId = roomId;

    this.webSocket.onmessage = (event) => {
      this.observers.forEach((observer) => observer.NewMessage(event.data));
    };
  }

  sendMessage(message) {
    this.webSocket.send(message);
  }

  close() {
    this.webSocket.close();
  }

  registerObserver(observer) {
    this.observers.push(observer);
  }

  unregisterObserver(observer) {
    var index = this.observers.indexOf(observer);
    if (index !== -1) {
      this.observers.splice(index, 1);
    }
  }
}
