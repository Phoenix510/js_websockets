const send_message_form = document.querySelector("#send_message-form");

async function sendMessage() {
  if (!ROOM_KEY || !USERNAME || !WEBSOCKETCONNETION) return;

  message_input = send_message_form.querySelector("#send_message-content");
  WEBSOCKETCONNETION.sendMessage(message_input.value);

  if (!message_input.value) return;

  send_message_form.reset();
}

send_message_form.addEventListener("submit", (event) => {
  event.preventDefault();
  sendMessage();
});
