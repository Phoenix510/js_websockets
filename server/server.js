// based on https://github.com/websockets/ws
import { WebSocketServer } from "ws";

const port = 8080;
const wss = new WebSocketServer({ port: port });
var rooms = {};

wss.on("connection", function connection(ws, request) {
  const urlParams = new URLSearchParams(request.url.slice(1));
  const roomId = urlParams.get("roomId");
  const ipAddress = request.socket.remoteAddress;
  ws.isAlive = true;
  ws.ipAddress = ipAddress;
  ws.username = `${urlParams.get("username")}:${generate_uuidv4()}`;

  if (!roomId) {
    handleInvalidRoomId(ws);
    return;
  }

  connectToRoom(ws, roomId, ipAddress);
  var messageObj = new Object();
  messageObj.type = "Username";
  messageObj.value = ws.username;
  ws.send(JSON.stringify(messageObj));

  ws.on("error", console.error);
  ws.on("close", () => {
    handleOnClose(ws);
  });
  ws.on("message", function message(data) {
    handleOnMessage(ws, data);
  });
  ws.on("pong", () => {
    heartbeat(ws);
  });
});

function handleOnClose(ws) {
  const room = rooms[ws.roomId];

  if (!room) return;
  else if (room.length > 1) {
    var index = room.indexOf(ws);
    if (index !== -1) {
      room.splice(index, 1);
    }
    return;
  }

  console.log("Closing room", ws.roomId);
  delete rooms[ws.roomId];
  console.log("Remaining active rooms:", Object.keys(rooms).length);
}

function handleOnMessage(ws, data) {
  const room = rooms[ws.roomId];

  if (!room) {
    handleInvalidRoomId(ws, ws.roomId);
    return;
  }

  var messageObj = new Object();
  messageObj.type = "Message";
  messageObj.username = ws.username;
  messageObj.date = new Date().toString();
  messageObj.content = data;
  var jsonString = JSON.stringify(messageObj);
  room.forEach((client) => client.send(jsonString));
}

function handleInvalidRoomId(ws, roomId) {
  const message = `Invalid room id: ${roomId}`;
  console.log(message);
  ws.send(message);
  ws.close();
}

function connectToRoom(ws, roomId, ipAddress) {
  let room = rooms[roomId];
  if (!room) {
    room = [];
    rooms[roomId] = room;
    console.log("New room with Id", roomId);
  }
  ws.roomId = roomId;
  room.push(ws);
  console.log(
    `New connection to room "${roomId}" from ${ipAddress}\nConnected clients: ${room.length}`
  );
}

function heartbeat(ws) {
  ws.isAlive = true;
}

function clearBrokenConnections() {
  const interval = setInterval(function ping() {
    wss.clients.forEach(function each(ws) {
      if (ws.isAlive === false) {
        console.log("Broken connection for ws with ip", ws.ipAddress);
        handleOnClose();
        return ws.terminate();
      }

      ws.isAlive = false;
      ws.ping();
    });
  }, 30000);
}

function generate_uuidv4() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var uuid = (Math.random() * 16) | 0,
      v = c == "x" ? uuid : (uuid & 0x3) | 0x8;
    return uuid.toString(16);
  });
}

console.log("Listening on:", port);
clearBrokenConnections();
