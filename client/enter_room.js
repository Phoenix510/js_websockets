const enter_room_form = document.querySelector("#enter_room-form");

async function enterRoom() {
  ROOM_KEY = enter_room_form.querySelector("#login-key").value;
  USERNAME = enter_room_form.querySelector("#login-username").value;

  document.querySelector("#room-key").innerHTML = `Room key ${ROOM_KEY}`;

  WEBSOCKETCONNETION = new WebSocketConnection(SERVER_IP_ADDRESS);
  WEBSOCKETCONNETION.connect(ROOM_KEY, USERNAME);
  WEBSOCKETCONNETION.registerObserver(new MessageHandler());

  enter_room_form.remove();
}

enter_room_form.addEventListener("submit", (event) => {
  event.preventDefault();
  enterRoom();
});
