class MessageHandler {
  NewMessage(data) {
    const messageObj = JSON.parse(data);
    switch (messageObj.type) {
      case "Username":
        this.refreshUsername(messageObj);
        break;
      case "Message":
        this.renderMessage(messageObj);
    }
  }

  refreshUsername(messageObj) {
    USERNAME = messageObj.value;
    document.querySelector(
      "#welcome-nav"
    ).innerHTML = `Welcome ${messageObj.value}`;
  }

  renderMessage(messageObj) {
    const container = document.querySelector("#room-messages_container");
    const newMessageCont = document.createElement("li");
    newMessageCont.classList.add("message");

    if (messageObj.username === USERNAME)
      newMessageCont.classList.add("message-owner");

    const dateElem = document.createElement("div");
    dateElem.classList.add("date");
    dateElem.innerHTML = new Date(messageObj.date).toLocaleString();

    const senderElem = document.createElement("div");
    senderElem.classList.add("sender");
    senderElem.innerHTML = messageObj.username;

    const contentElem = document.createElement("div");
    contentElem.classList.add("content");
    contentElem.innerHTML = String.fromCharCode(...messageObj.content.data);

    newMessageCont.appendChild(senderElem);
    newMessageCont.appendChild(dateElem);
    newMessageCont.appendChild(contentElem);

    container.insertBefore(newMessageCont, container.firstChild);
  }
}
